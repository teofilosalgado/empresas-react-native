import React from 'react';
import { Provider, connect } from 'react-redux';
import AppStore from './store/App'

import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';

import SearchScreen from './screens/SearchScreen'
import LoginScreen from './screens/LoginScreen'
import AboutScreen from './screens/AboutScreen'
import HomeScreen from './screens/HomeScreen'
import Bootstrap from './screens/Bootstrap'

const MainNavigator = createStackNavigator({
  home:
  {
    screen: HomeScreen
  },
  search:
  {
    screen: SearchScreen
  },
  about:
  {
    screen: AboutScreen
  }
});

const InitialNavigator = createSwitchNavigator({
  bootstrap:
  {
    screen: Bootstrap
  },
  login:
  {
    screen: LoginScreen,
  },
  app:
  {
    screen: MainNavigator,
  }
});

const Navigation = createAppContainer(InitialNavigator);

class App extends React.Component {
  render() {
    return (
      <Provider store={AppStore}>
        <Navigation>
        </Navigation>
      </Provider>
    )
  }
}

export default App;