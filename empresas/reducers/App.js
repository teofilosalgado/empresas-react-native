const initialState = {
  investor:
  {

  },
  login:
  {
    uiState: true,
    success: false,
  },
  search:
  {
    results: []
  },
  about:
  {
    uiState: false
  }
}

export default function rootReducer(state = initialState, action) {
  if (action.type === "SIGNOUT") {
    return {
      investor:
      {

      },
      login:
      {
        uiState: true,
        success: false,
      },
      search:
      {
        results: []
      },
      about:
      {
        uiState: false
      }
    }
  }

  if (action.type === "SIGNIN_STARTED") {
    return { ...state, login: { uiState: false } }
  }
  if (action.type === "SIGNIN_SUCCESS") {
    return { ...state, investor: action.investor, login: { success: true } }
  }
  if (action.type === "SIGNIN_CREDENTIALS_ERROR") {
    alert("Senha ou email incorretos!")
    return { ...state, login: { uiState: true } }
  }
  if (action.type === "SIGNIN_CONNECTION_ERROR") {
    alert("Verifique sua conexão!")
    return { ...state, login: { uiState: true } }
  }

  if (action.type === "SEARCH_STARTED") {
    return { ...state, search: { results: [] } }
  }
  if (action.type === "SEARCH_SUCCESS") {
    return { ...state, search: { results: action.results } }
  }

  if (action.type === "SHOW_STARTED") {
    return { ...state, about: {} }
  }
  if (action.type === "SHOW_SUCCESS") {
    return { ...state, about: { uiState: true, data: action.data } }
  }

  if (action.type === "API_CREDENTIAL_ERROR") {
    alert("Credenciais inválidas!")
    return { ...state, login: { success: false } }
  }
  if (action.type === "API_CONNECTION_ERROR") {
    alert("Verifique sua conexão!")
    return state;
  }

  return state;
};