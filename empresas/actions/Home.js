import { SecureStore } from 'expo';

export function signout(id) {
  return async dispatch => {
    const headers = ["access-token", "uid", "client"]
    headers.forEach(header => {
      SecureStore.setItemAsync(header, "")
    });
    dispatch({ type: "SIGNOUT" })
  }
}