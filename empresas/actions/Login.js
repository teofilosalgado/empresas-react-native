import axios from 'axios'
import { SecureStore } from 'expo';

export function signin(email, password) {
  return async dispatch => {
    try {
      const response = await axios.post("http://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
        {
          email: email,
          password: password
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        })

      const headers = ["access-token", "uid", "client"]
      headers.forEach(header => {
        SecureStore.setItemAsync(header, response.headers[header])
      });

      dispatch({ type: "SIGNIN_SUCCESS", investor: response.data.investor })
    }
    catch (error) {
      if (error.response.status == 401) {
        dispatch({ type: "SIGNIN_CREDENTIALS_ERROR" })
      }
      else {
        dispatch({ type: "SIGNIN_CONNECTION_ERROR" })
      }
    }
  }
}