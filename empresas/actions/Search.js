import axios from 'axios'
import { SecureStore } from 'expo';

export function find(query, filter) {
  return async dispatch => {
    dispatch({ type: "SEARCH_STARTED" })
    try {

      var body = {};

      if (!filter) {
        body = {
          name: query
        }
      }
      else if (!query) {
        body = {
          enterprise_types: filter,
        }
      }
      else if (!query && !filter) {

      }
      else {
        body = {
          name: query,
          enterprise_types: filter
        }
      }

      const a = await SecureStore.getItemAsync("access-token")
      const c = await SecureStore.getItemAsync("client")
      const u = await SecureStore.getItemAsync("uid")

      const response = await axios.get('http://empresas.ioasys.com.br/api/v1/enterprises', {
        params: body,
        headers: {
          "access-token": a,
          "client": c,
          "uid": u
        }
      })
      dispatch({ type: "SEARCH_SUCCESS", results: response.data.enterprises })
    }
    catch (error) {
      if (error.response.status == 401) {
        const headers = ["access-token", "uid", "client"]
        headers.forEach(header => {
          SecureStore.setItemAsync(header, "")
        });
        dispatch({ type: "API_CREDENTIAL_ERROR" })
      }
      else {
        dispatch({ type: "API_CONNECTION_ERROR" })
      }
    }
  }
}