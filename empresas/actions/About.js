import axios from 'axios'
import { SecureStore } from 'expo';

export function show(id) {
  return async dispatch => {
    dispatch({ type: "SHOW_STARTED" })
    try {
      const a = await SecureStore.getItemAsync("access-token")
      const c = await SecureStore.getItemAsync("client")
      const u = await SecureStore.getItemAsync("uid")

      const response = await axios.get('http://empresas.ioasys.com.br/api/v1/enterprises/' + id, {
        headers: {
          "access-token": a,
          "client": c,
          "uid": u
        }
      })
      dispatch({ type: "SHOW_SUCCESS", data: response.data.enterprise })
    }
    catch (error) {
      if (error.response.status == 401) {
        const headers = ["access-token", "uid", "client"]
        headers.forEach(header => {
          SecureStore.setItemAsync(header, "")
        });
        dispatch({ type: "API_CREDENTIAL_ERROR" })
      }
      else {
        dispatch({ type: "API_CONNECTION_ERROR" })
      }
    }
  }
}