import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View, Text, StyleSheet, TextInput, ScrollView, FlatList } from 'react-native'

import Card from './../components/Card'
import Search from './../components/Search'

import { find } from './../actions/Search'

function mapStateToProps(state) {
  return {
    search: state.search,
    login: state.login
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ find: find }, dispatch)
}

class SearchScreen extends React.Component {
  static navigationOptions = {
    title: 'Pesquisar',
  };

  constructor(props) {
    super(props)
    this.state = {
      searchBarValue: this.props.navigation.getParam('query', ''),
      filterBarValue: ""
    }
    this.searchOnChangeText = this.searchOnChangeText.bind(this)
    this.filterOnChangeText = this.filterOnChangeText.bind(this)
  }

  componentDidMount() {
    this.props.find(this.state.searchBarValue, this.state.filterBarValue)
  }


  componentDidUpdate()
  {
    if (!this.props.login.success) {
      this.props.navigation.navigate("login")
    }
  }

  searchOnChangeText(value) {
    this.props.find(value, this.state.filterBarValue)
    this.setState({ searchBarValue: value })
  }

  filterOnChangeText(value) {
    this.props.find(this.state.searchBarValue, value)
    this.setState({ filterBarValue: value })
  }

  cardItem = ({ item }) => (
    <Card style={styles.card} title={item.enterprise_name} onPress={() => { this.props.navigation.navigate("about", { id: item.id }) }}>
      <Text style={{ marginTop: -10, fontSize: 14 }}>{item.city}, {item.country}</Text>
      <View style={{ flexDirection: "row", marginTop: 10 }}>
        <Text style={{ fontSize: 18 }}>Área de atuação: </Text>
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>{item.enterprise_type.enterprise_type_name}</Text>
      </View>
      <View style={{ marginTop: 5, flexDirection: "column", alignItems: "center", justifyContent: "center" }}>
        <Text style={{ fontSize: 14, fontWeight: "bold", marginTop: 5 }}>Preço da ação:</Text>
        <Text style={{ fontSize: 40, fontWeight: "bold" }} >R$ {item.share_price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
      </View>
    </Card>
  )

  render() {
    return (
      <ScrollView contentContainerStyle={styles.base}>
        <View style={styles.container}>
          <Search value={this.state.searchBarValue} onChangeText={this.searchOnChangeText}></Search>
          <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-start", height: 50 }}>
            <MaterialCommunityIcons name={"filter"} color={"#888888"} size={30}></MaterialCommunityIcons>
            <Text style={{ color: "#888888", marginLeft: 10 }}>Filtrar por:</Text>
            <TextInput value={this.state.fitlerBarValue} onChangeText={this.filterOnChangeText} style={{ marginLeft: 10, height: 40, flexGrow: 1, flex: 1, borderBottomColor: "#888888", width: "100%", borderBottomWidth: 2 }} placeholder={"tipo de empresa"}></TextInput>
          </View>

          <FlatList data={this.props.search.results} renderItem={this.cardItem} keyExtractor={(item, index) => item.id.toString()}>

          </FlatList>

        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  base: {
    backgroundColor: '#F0F0F0',
    minHeight: "100%"
  },
  container: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    padding: 20,
  },
  title: {
    fontFamily: "poppins-bold",
    color: "#FFFFFF",
    fontSize: 24,
    marginTop: 30
  },
  balance: {
    color: "#388E3C",
    fontSize: 30,
    fontWeight: '500'
  },
  card: {
    marginTop: 20
  }
});

export default connect(mapStateToProps, matchDispatchToProps)(SearchScreen);