import React from 'react';
import { Font } from 'expo';
import { SecureStore } from 'expo';
import { StyleSheet, View} from 'react-native';

export default class Bootstrap extends React.Component {
  constructor(props) {
    super(props);
    this.loadAssets();
  }

  async loadAssets() {
    await Font.loadAsync({
      'poppins-bold': require('./../assets/fonts/Poppins-Bold.ttf'),
    });
    this.props.navigation.navigate('login')
  }

  render() {
    return(
      <View style={{flex: 1,backgroundColor: '#E51F6B', height:"100%", width:"100%"}}>

      </View>
    );
  }
}  