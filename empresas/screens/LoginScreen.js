import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { StyleSheet, Text, View, KeyboardAvoidingView, TextInput, ActivityIndicator } from 'react-native';
import FloatingActionButton from './../components/FloatingActionButton'

import { signin } from './../actions/Login'

function mapStateToProps(state) {
  return {
    login: state.login
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ signin: signin }, dispatch)
}

class LoginScreen extends React.Component {
  static navigationOptions = {
    title: 'Login',
    header: null
  };

  constructor(props) {
    super(props)
    this.state = {
      email: "testeapple@ioasys.com.br",
      password: "12341234",
    }
  }

  componentDidUpdate() {
    if (this.props.login.success == true) {
      this.props.navigation.navigate('app')
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Entrar</Text>
        <KeyboardAvoidingView behavior="padding" enabled>

          <View>
            <Text style={styles.tooltip}>email</Text>
            <TextInput autoComplete={"email"} value={this.state.email} onChangeText={(email) => this.setState({ email })} style={styles.textInput} multiline={false} selectionColor={'#FFFFFF'}></TextInput>
          </View>

          <View style={{ marginTop: 15 }}>
            <Text style={styles.tooltip}>password</Text>
            <TextInput value={this.state.password} onChangeText={(password) => this.setState({ password })} style={styles.textInput} secureTextEntry={true} multiline={false} selectionColor={'#FFFFFF'}></TextInput>
          </View>

        </KeyboardAvoidingView>
        <FloatingActionButton enabled={this.props.login.uiState} onPress={() => { this.props.signin(this.state.email, this.state.password) }}>
        </FloatingActionButton>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E51F6B',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    padding: 20,
  },
  title: {
    fontFamily: "poppins-bold",
    color: "#FFFFFF",
    fontSize: 24,
    marginTop: 30
  },

  tooltip: {
    color: "#FFFFFF",
    fontSize: 16
  },

  textInput: {
    height: 40,
    width: "100%",
    borderBottomColor: "#FFFFFF",
    borderBottomWidth: 2,
    color: "#FFFFFF",
  }
});

export default connect(mapStateToProps, matchDispatchToProps)(LoginScreen);