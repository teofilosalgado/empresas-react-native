import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View, ScrollView, Text, StyleSheet, Image } from 'react-native'

import { show } from './../actions/About'

function mapStateToProps(state) {
  return {
    about: state.about,
    login: state.login
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators({ show: show }, dispatch)
}

class Item extends React.Component {
  render() {
    return (
      <View style={this.props.style}>
        <Text style={{ color: "#E51F6B", fontSize: 18, fontWeight: "bold" }}>{this.props.title}</Text>
        <Text style={{ fontSize: 40, fontWeight: "bold" }}>{this.props.content}</Text>
      </View>
    )
  }
}

class AboutScreen extends React.Component {
  static navigationOptions = {
    title: 'Sobre',
  };

  constructor(props) {
    super(props)
    state = {
      id: this.props.navigation.getParam('id', '').toString()
    }
    this.props.show(this.props.navigation.getParam('id', '').toString())
  }

  componentDidUpdate()
  {
    if (!this.props.login.success) {
      this.props.navigation.navigate("login")
    }
  }

  render() {
    if (this.props.about.uiState) {
      return (
        <ScrollView contentContainerStyle={styles.base}>
          <View style={styles.container}>
            <Text style={{ fontFamily: "poppins-bold", fontSize: 38, color: "#E51F6B" }}>
              {this.props.about.data.enterprise_name}
            </Text>
            <Text style={{ marginTop: -10, fontSize: 14 }}>
              {this.props.about.data.city}, {this.props.about.data.country}
            </Text>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ fontSize: 18 }}>Área de atuação: </Text>
              <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                {this.props.about.data.enterprise_type.enterprise_type_name}
              </Text>
            </View>
          </View>

          <View style={{ height: 200, backgroundColor: "#C4C4C4" }}>
            <Image style={{ width: "100%", height: "100%" }} resizeMode={"cover"} source={{ uri: "http://empresas.ioasys.com.br" + this.props.about.data.photo }}></Image>
          </View>

          <View style={styles.container}>
            <Item title={"Valor da ação:"} content={"R$ " + this.props.about.data.share_price} ></Item>
            <Item style={{ marginTop: 5 }} title={"Ações disponíveis"} content={this.props.about.data.shares}></Item>

            <Text style={{ fontSize: 16, width: "100%", marginTop: 10 }}>
              {this.props.about.data.description}
            </Text>
          </View>
        </ScrollView>
      )
    }
    else {
      return (
        <View style={styles.base}>

        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  base: {
    backgroundColor: '#F0F0F0',
    minHeight: "100%"
  },
  container: {
    padding: 20,
  },
});

export default connect(mapStateToProps, matchDispatchToProps)(AboutScreen);