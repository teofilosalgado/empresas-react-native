import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { MaterialIcons } from '@expo/vector-icons';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Image } from 'react-native'

import Card from './../components/Card'
import Search from './../components/Search'

import { signout } from './../actions/Home'

function mapStateToProps(state) {
  return {
    investor: state.investor,
  }
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators({ signout: signout }, dispatch)
}


class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      searchBarValue: ""
    }
    this.searchOnChangeText = this.searchOnChangeText.bind(this)
  }

  searchOnChangeText(value) {
    this.setState({ searchBarValue: value })
  }

  render() {
    return (
      <ScrollView contentContainerStyle={styles.base}>
        <View style={styles.container}>
          <View>
            <View style={[styles.card, { alignItems: 'center', justifyContent: 'flex-start', flexDirection: "row" }]}>
              <View style={{ height: 70, width: 70, backgroundColor: "#C4C4C4", borderRadius: 35, alignItems: 'center', justifyContent: 'center' }}>
                {this.props.investor.photo === null ? <MaterialIcons name={"account-circle"} size={60} style={{ color: "#FFFFFF" }}></MaterialIcons> : <Image style={{ width: "100%", height: "100%" }} resizeMode={"cover"} source={{ uri: "http://empresas.ioasys.com.br" + this.props.investor.photo }}></Image>}
              </View>
              <View style={{ marginLeft: 20, alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: "column" }}>
                <Text style={{ fontFamily: "poppins-bold", fontSize: 20 }}>{this.props.investor.investor_name}</Text>
                <Text>{this.props.investor.city}, {this.props.investor.country}</Text>
              </View>
            </View>
          </View>

          <Search style={styles.card} onChangeText={this.searchOnChangeText} onSubmitEditing={() => { this.props.navigation.navigate('search', { query: this.state.searchBarValue }) }}></Search>

          <Card title={"Saldo"} style={styles.card}>
            <Text style={styles.balance}>R$ {this.props.investor.balance.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
          </Card>

          <Card title={"Portfolio"} style={styles.card} button={true} onPress={() => this.props.navigation.navigate('search')} buttonTitle={"conheça nossas opções"}>
            <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start', flexDirection: "row", marginBottom: 10 }}>
              <View style={{ height: 50 }} >
                <MaterialIcons name={"sentiment-very-satisfied"} size={70} style={{ color: "#C4C4C4" }}></MaterialIcons>
              </View>
              <View style={{ paddingLeft: 10, width: 0, flexGrow: 1, flex: 1, height: "100%" }}>
                <Text style={{ fontWeight: "bold", fontSize: 18, color: "#C4C4C4" }}>Que tal começar seu portfolio agora mesmo?</Text>
              </View>
            </View>
          </Card>

          <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center', marginTop: 20 }} onPress={() => {
            this.props.signout()
            this.props.navigation.navigate("login")
          }}>
            <Text style={{ color: "#E51F6B", fontWeight: "bold", fontSize: 16 }}>sair</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  base: {
    backgroundColor: '#F0F0F0',
    minHeight: "100%"
  },
  container: {
    flex: 1,
    backgroundColor: '#F0F0F0',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    padding: 20,
  },
  title: {
    fontFamily: "poppins-bold",
    color: "#FFFFFF",
    fontSize: 24,
    marginTop: 30
  },
  balance: {
    color: "#388E3C",
    fontSize: 30,
    fontWeight: '500'
  },
  card: {
    marginTop: 20
  }
});

export default connect(mapStateToProps, matchDispatchToProps)(HomeScreen);