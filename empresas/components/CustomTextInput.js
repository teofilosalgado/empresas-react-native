import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

export default class CustomTextInput extends React.Component {
  render() {
    return (
      <View style={this.props.style}>
        <Text style={styles.tooltip}>{this.props.tooltip}</Text>
        <TextInput defaultValue={this.props.defaultValue} value={this.props.value} onChangeText={this.props.onChange} style={styles.textInput} secureTextEntry={this.props.secureTextEntry} multiline={false} keyboardType={this.props.keyboardType} selectionColor={'#FFFFFF'}></TextInput>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tooltip: {
    color: "#FFFFFF",
    fontSize: 16
  },

  textInput: {
    height: 40,
    width: "100%",
    borderBottomColor: "#FFFFFF",
    borderBottomWidth: 2,
    color: "#FFFFFF",
  }
});