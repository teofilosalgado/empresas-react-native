import React from 'react';
import { StyleSheet, KeyboardAvoidingView, View, Platform, TouchableNativeFeedback, TouchableHighlight } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class FloatingActionButton extends React.Component {
  render() {
    const Content = <View style={styles.button}>
      <MaterialCommunityIcons name={"chevron-right"} size={45} style={{ color: "#E51F6B" }} />
    </View>


    if (Platform.OS === 'android') {
      return (
        <KeyboardAvoidingView style={styles.buttonView} behavior="padding" enabled>
          <TouchableNativeFeedback onPress={(this.props.enabled === true) ? this.props.onPress : null}>
            {Content}
          </TouchableNativeFeedback>
        </KeyboardAvoidingView>
      )
    }
    else {
      return (
        <KeyboardAvoidingView style={styles.buttonView} behavior="padding" enabled>
          <TouchableHighlight onPress={(this.props.enabled === true) ? this.props.onPress : null}>
            {Content}
          </TouchableHighlight>
        </KeyboardAvoidingView>
      )
    }
  }
}

const styles = StyleSheet.create({
  buttonView:
  {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  button:
  {
    elevation: 5,
    backgroundColor: "#FFFFFF",
    height: 60,
    width: 60,
    marginBottom: 10,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  }
});