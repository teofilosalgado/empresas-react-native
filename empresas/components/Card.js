import React from 'react';
import { View, Text, StyleSheet, Button, Platform, TouchableNativeFeedback, TouchableHighlight } from 'react-native'

export default class Card extends React.Component {
  render() {
    const Content = <View style={[styles.background, this.props.style]}>
      <View style={{ padding: 20 }}>
        <Text style={styles.title}>{this.props.title}</Text>
        {this.props.children}
      </View>
      {this.props.button === true ? <Button color="#E51F6B" onPress={this.props.onPress} style={{ height: 50 }} title={this.props.buttonTitle}  ></Button> : null}
    </View>

    if (Platform.OS === 'android') {
      return (
        <TouchableNativeFeedback onPress={this.props.onPress}>
          {Content}
        </TouchableNativeFeedback>
      )
    }
    else {
      return (
        <TouchableHighlight onPress={this.props.onPress}>
          {Content}
        </TouchableHighlight>
      )
    }
  }
}

const styles = StyleSheet.create({
  background: {
    elevation: 1,
    backgroundColor: "#FFFFFF",
    width: "100%",
    borderRadius: 3
  },
  title: {
    fontFamily: "poppins-bold",
    color: "#E51F6B",
    fontSize: 18,
    marginBottom: 5
  }
})