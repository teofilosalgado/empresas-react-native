import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons';

export default class Card extends React.Component {

  constructor(props) {
    super(props)
  }
  render() {
    return (
      <View style={[styles.background, this.props.style]}>
        <MaterialIcons
          name={"search"}
          size={24}
          style={{ color: "#E51F6B" }}
        />
        <TextInput value={this.props.value} onChangeText={this.props.onChangeText} clearButtonMode="while-editing" onSubmitEditing={this.props.onSubmitEditing} style={{ width: "100%", height: 40, paddingLeft: 10, paddingRight: 20 }} multiline={false} placeholder={"Buscar empresas"}></TextInput>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    elevation: 1,
    backgroundColor: "#FFFFFF",
    height: 50,
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20,
    borderRadius: 3,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-start"
  },
})