# Aplicativo empresas
Este *README* foi dividido nos seguintes tópicos:

  - **Design, guidelines e planejamentos**
  
  - **Bibliotecas utilizadas**
  
  - **Como executar esta aplicação**
  
  - **Considerações finais e resultados**

### Design, guidelines e planejamentos:
Antes do desenvolvimento do código que compõe esta aplicação projetei o esboço do que seriam as telas e interações dentro do aplicativo utilizando a ferramenta online **Figma**. Para manter o design coeso foi empregado o *Material Design* como guideline durante o processo criativo. O protótipo pode ser visualizado através deste [link](https://www.figma.com/file/0Z7muGaHVwEHijhlG9QlJUQI/Empresas).

### Bibliotecas utilizadas:
Nesta aplicação foram implementadas as seguintes bibliotecas:

- **axios**  
Utilizado para a execução de requisições HTTP baseada em *promises*.
- **expo**  
Utilizado para agilizar e facilitar o desenvolvimento e teste em vários dispositivos.
- **react-navigation**  
Utilizado para a construção das rotas e navegação dentro da aplicação
- **redux (react-redux)**  
Utilizado para a instituição de um container de estado previsível, disponível para os componentes acessarem durante a execução.
- **redux-thunk**  
Middleware utilizado para a execução de atualizações assíncronas que interagem com a store.

### Como executar esta aplicação
- ##### Via expo 
    - **Android** 
    Instale o aplicativo Expo, inicie-o e escaneie o QR Code neste [link](https://expo.io/@kawabungaxdg/empresas)
    - **iOS**
    Instale o aplicativo Expo no seu telefone e siga as instruções neste [link](https://expo.io/@kawabungaxdg/empresas) para encontrar este aplicativo.  
	
- ##### Instalando e compilando localmente

    1 - Instale o NodeJS e execute:
	
    ```
    npm install expo-cli --global
    ```
	
    2 - Clone o respositório
	
    3 - Dentro da pasta *empresas* execute:
	
    ```
    expo start
    ```
	
    4 - Instale em seu telefone o aplicativo Expo, inicie-o e escaneie o QR Code que será exibido no seu terminal e navegador.
	
### Considerações finais e resultados
Fazendo uso dos endpoints disponíveis foi possível desenvolver esta aplicação de gerenciamento de portfólio empresarial tendo em vista uma boa usabilidade, design e experiência do usuário. O aplicativo cumpre todos os requisitos apresentados nas instruções para desenvolvimento do mesmo.